from __future__ import print_function 
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
np.random.seed(1)
# DATA
N = 100
K = 5

X = np.random.randint(1000, 1100, 100).reshape(100, 1)
Y = np.random.randint(1, 100, 100).reshape(100, 1)

Xd = np.concatenate((X, Y), axis = 1)

M = int(N / K)

labels = np.asarray([0] * M + [1] * M + [2] * M + [3] * M + [4] * M)

plt.title("Bảng Số Liệu")
for k in range(K):
    plt.scatter(Xd[labels == k, 0], Xd[labels == k, 1])
plt.show()

# MYCDIST
def mycdist(Xd, centers):
    D = np.zeros((Xd.shape[0], centers.shape[0]))
    for i in range(D.shape[0]):
        for j in range(D.shape[1]):
            D[i][j] = (((Xd[i][0] - centers[j][0]) ** 2) / 4 + ((Xd[i][1] - centers[j][1]) ** 2) / 1) ** (1/2)
    return D

# INITIALIZE MEANS
def init_means(Xd, K):
    return Xd[np.random.choice(Xd.shape[0], K, replace=False)]

# UPDATE LABELS
def update_labels_1(Xd, centers):
    D = mycdist(Xd, centers)
    return np.argmin(D, axis = 1)
def updated_labels_2(Xd, centers):
    D = cdist(Xd, centers)
    return np.argmin(D, axis = 1)

# UPDATE CENTERS
def update_centers(Xd, labels, K):
    centers = np.zeros((K, Xd.shape[1]))
    for k in range(K):
        xk = Xd[labels == k, :]
        centers[k, :] = np.mean(xk, axis = 0)
    return centers

# CHECK TO STOP
def check(centers, new_centers):
    return (set([tuple(a) for a in centers]) == 
        set([tuple(a) for a in new_centers]))

# KMEANS
def kmeans_mycdist(Xd, K):
    centers = [init_means(Xd, K)]
    labels = []
    while True:
        labels.append(update_labels_1(Xd, centers[-1]))
        new_centers = update_centers(Xd, labels[-1], K)
        isDone = check(new_centers, centers[-1])
        if isDone:
            break
        centers.append(new_centers)
    return (centers[-1], labels[-1])

(centers, labels) = kmeans_mycdist(Xd, K)

print("Centers found: \n" + str(centers))

plt.title("Bảng đã qua xử lý với trọng số khác")
for k in range(K):
    plt.scatter(Xd[labels == k, 0], Xd[labels == k, 1])
plt.show()

def kmeans_cdist(Xd, K):
    centers = [init_means(Xd, K)]
    labels = []
    while True:
        labels.append(update_labels_2(Xd, centers[-1]))
        new_centers = update_centers(Xd, labels[-1], K)
        isDone = check(new_centers, centers[-1])
        if isDone:
            break
        centers.append(new_centers)
    return (centers[-1], labels[-1])

(centers, labels) = kmeans_mycdist(Xd, K)

print("Centers found: \n" + str(centers))

plt.title("Bảng đã qua xử lý")
for k in range(K):
    plt.scatter(Xd[labels == k, 0], Xd[labels == k, 1])
plt.show()

