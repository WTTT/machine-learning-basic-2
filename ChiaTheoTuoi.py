import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist


# DATA
Y = (np.random.randint(0, 40, 70)).reshape(1, 70)
Z = (np.random.randint(65, 70, 25)).reshape(1, 25)
T = (np.random.randint(71, 100, 5)).reshape(1, 5)
Y = np.concatenate((Y, Z, T), axis = 1)
# Y = np.sort(Y)

X = (np.zeros(100)).reshape(1, 100)

# INFO
K = 4 # số clusters cần chia
N = 100 # số lượng điểm dữ liệu = Y.shape[0]
M = int(N / K)

Y = np.concatenate((X, Y), axis = 0).T
# print(Y)
labels = np.asarray([0] * M + [1] * M + [2] * M + [3] * M)

plt.title("Biểu diễn dữ liệu theo kiểu gốc / 1 chiều")
plt.xlabel("N")
plt.ylabel("AGE")
for k in range(K):
    plt.scatter(Y[labels == k, 0], Y[labels == k, 1])
plt.show()

# INITIALIZE NEW CENTERS
def init_centers(Y, K):
    return Y[np.random.choice(Y.shape[0], K)]

# UPDATE LABELS
def labels_updated(centers, Y):
    D = cdist(Y, centers)
    return np.argmin(D, axis = 1)

# UPDATE CENTERS
def centers_updated(labels, Y, K):
    centers = np.zeros((K, Y.shape[1]))
    for k in range(K):
        yk = Y[labels == k, :]
        centers[k, :] = np.mean(yk, axis = 0, dtype=float)
    return centers

# CHECK TO STOP
def check(centers, new_centers):
    return (set([tuple(a) for a in centers]) == 
        set([tuple(a) for a in new_centers]))

# KMEANS
def kmeans(Y, K):
    centers = [init_centers(Y, K)]
    labels = []
    while True:
        labels.append(labels_updated(centers[-1], Y))
        new_centers = centers_updated(labels[-1], Y, K)
        # for k in range(Y.shape[0]):
        #     if labels[-1][k] == 0:
        #         plt.plot([k], Y[k][1], 'ro')
        #     if labels[-1][k] == 1:
        #         plt.plot([k], Y[k][1], 'ko')
        #     if labels[-1][k] == 2:
        #         plt.plot([k], Y[k][1], 'go')
        #     if labels[-1][k] == 3:
        #         plt.plot([k], Y[k][1], 'yo')
        # plt.show()
        if check(centers[-1], new_centers):
            break
        centers.append(new_centers)
    return (centers[-1], labels[-1])

(centers, labels) = kmeans(Y, K)

print("Centers found: \n" + str(centers) + "\n")

plt.title("Biểu diễn trên tọa độ XOY")
plt.xlabel("N")
plt.ylabel("AGE")

for k in range(Y.shape[0]):
    if labels[k] == 0:
        plt.plot([k], Y[k][1], 'ro')
    if labels[k] == 1:
        plt.plot([k], Y[k][1], 'ko')
    if labels[k] == 2:
        plt.plot([k], Y[k][1], 'go')
    if labels[k] == 3:
        plt.plot([k], Y[k][1], 'yo')
plt.show()

plt.title("Biểu diễn trên 1 chiều")
for k in range(K):
    plt.scatter(Y[labels == k, 1], Y[labels == k , 0])
plt.show()


for k in range(K):
    print("Tuổi nhóm " + str(k+1) + ":\n" + str(np.sort(Y[labels == k, 1])) + "\n")

"""
    Dứ liệu chỉ có số tuổi nên x là zero với mọi điểm dữ liệu
"""
