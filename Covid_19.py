from bs4 import BeautifulSoup
import urllib.request
import numpy as np
import matplotlib.pyplot as plt

# url =  'https://www.nytimes.com/interactive/2020/us/coronavirus-us-cases.html'
# page = urllib.request.urlopen(url)
# soup = BeautifulSoup(page, 'html.parser')

# # CASES
# cases = soup.find_all('rect', class_ = 'cases svelte-1w4fc87')

# data_cases = []

# for case in cases:
#     scase = str(case).split(" ")
#     height = scase[3]
#     hh = height.index('"')
#     real_height = float(height[hh + 1:(len(height) - 1)])
#     data_cases.append(real_height)

# last_height = data_cases[-1]
# last_cases = 18857

# ratio = last_cases / last_height

# for i in range(len(data_cases)):
#     data_cases[i] = data_cases[i] * ratio

# data_cases = data_cases[0:(int(len(data_cases)/2))]

# # DEATHS
# deaths = soup.find_all('rect', class_ = 'deaths')
# data_deaths = []
# for death in deaths:
#     sdeath = str(death).split(" ")
#     height = sdeath[3]
#     hh = height.index('"')
#     real_height = float(height[hh + 1:(len(height) - 1)])
#     data_deaths.append(real_height)

# last_height = data_deaths[-1]
# last_deaths = 747

# ratio = last_deaths / last_height

# for i in range(len(data_deaths)):
#     data_deaths[i] *= ratio

# days = [i for i in range(len(data_deaths))]

# plt.plot(days[:35], data_cases[:35], 'ro')
# plt.title('Cases')
# plt.show()

# plt.plot(days, data_deaths, 'bo')
# plt.title('Deaths')
# plt.show()

# with open("C:/Users/DD/Desktop/Project/GitLab/machine-learning-basic-2/C19data.txt", "w") as f:
#     for i in range(35):
#         f.write(str(days[i]) + "|" + str(int(data_cases[i])) + "|" + str(int(data_deaths[i])) + "\n")

import pandas as pd
# READ DATA
path = "C:/Users/DD/Desktop/Project/GitLab/machine-learning-basic-2/C19data.txt"
r_cols = ["day", "new cases", "deaths"]
file = pd.read_csv(path, header=None, names=r_cols, sep="|")
data = np.asarray(file)
day = data[:, 0]
cases = data[:, 1]
deaths = data[:, 2]

# TRAINING DATA
day_train = day[:30]
cases_train = cases[:30]
deaths_train = deaths[:30]

# CASES
# Hàm dự đoán có bậc 2 hoặc 3
def predict(cases_train, day_train, day, cases, degree, data = "Cases"):
    degree -= 1
    Y = cases_train.reshape(cases_train.shape[0], 1)
    for i in range(degree): # thay range đê có thể cem các bậc khác nhau
        X = np.zeros((day_train.shape[0]))
        for n in range(day_train.shape[0]):
            X[n] = day_train[n]
        X = X.reshape(X.shape[0], 1)
        n_day_train = np.zeros((day_train.shape[0]))
        for j in range(0, i + 1):
            n_day_train = np.zeros((day_train.shape[0]))
            for k in range(n_day_train.shape[0]):
                n_day_train[k] = day_train[k] ** (j + 2)
            n_day_train = n_day_train.reshape(n_day_train.shape[0], 1)
            X = np.concatenate((X, n_day_train), axis = 1)
        ones = np.ones((X.shape[0], 1))
        Xbar = np.concatenate((ones, X), axis = 1)
        b = Xbar.T.dot(Y)
        A = Xbar.T.dot(Xbar)
        w = np.linalg.pinv(A).dot(b)
        x = np.linspace(0, 34, 35)
        y = np.linspace(0, 0, 35)
        for m in range(w.shape[0]):
            y += x ** m * w[m]
        N_pre = 5
        y_e = y[30:]
        cases_e = cases[30 :]
        error = 0
        per_e = 0
        for p in range(N_pre):
            error += (y_e[p] - cases_e[p]) ** 2
            per_e += abs(y_e[p] - cases_e[p]) / cases_e[p] * 100
        error = (error / N_pre) ** (1/2)
        per_e = 100 - per_e / N_pre
        plt.title("Bậc: " + str(i + 2) + "\nRMSE: " + str(int(error)) + " người, Độ chính xác trung bình " + str(round(per_e,2)) + "%")
        plt.xlabel("Days")
        plt.ylabel(data)
        plt.plot(day, cases, label="Tất cả dữ liệu" )
        plt.plot(day[30:], cases[30:], 'ro', label = "Dữ liệu thật")
        plt.plot(x, y, label = "Đường dự đoán")
        plt.legend()
        plt.show()
predict(cases_train, day_train, day, cases, degree = 4) # thay degree sẽ thấy được đến bậc degree 
predict(deaths_train, day_train, day, deaths, degree = 5, data="Deaths") # thay degree sẽ thấy được đến bậc degree # bậc 3 bậc 4 của deaths khá gần nhau
deaths_train[28] = 600
predict(deaths_train, day_train, day, deaths, degree = 10, data="Deaths")











