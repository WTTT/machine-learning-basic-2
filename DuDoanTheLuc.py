import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sympy.utilities.iterables import multiset_permutations
from itertools import combinations
from scipy.spatial.distance import cdist
from sklearn.metrics import mean_absolute_error
X = np.array([[50, 55, 60, 62, 66, 70, 76, 79, 84, 90]]).T
Y = np.array([[6000., 5500., 4900., 4300., 4000., 3500., 3200., 3000., 2700., 2300.]]).T
# X_true = np.array([[79, 84, 90]]).T
# Y_true = np.array([[3000, 2700, 2300]]).T
plt.title("Bảng Dữ Liệu Ban Đầu")
plt.xlabel("Tuổi")
plt.ylabel("Thể Lực")
plt.plot(X, Y, 'ro')
plt.show()


def linear_regression(X_data, Y_data, n = 5):
    r = [("No data", 1000000000)]
    for k in range(2, X_data.shape[0]):
        for c in list(combinations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], k)):
            Z = np.array(c)
            X = X_data[Z]
            Y = Y_data[Z]
            for i in range(1, n):
                Xp = np.ones((X.shape[0], X.shape[1]))
                n_X = np.zeros((X.shape[0], X.shape[1]))
                for m in range(1, i):
                    n_X = np.zeros((X.shape[0], X.shape[1]))
                    for j in range(0, X.shape[0]):
                        n_X[j] = X[j] ** m
                    Xp = np.concatenate((Xp, n_X), axis = 1)
                Xbar = Xp
                A = Xbar.T.dot(Xbar)
                b = Xbar.T.dot(Y)
                w = np.linalg.pinv(A).dot(b)
                x = np.linspace(50, 90, 10000)
                y = np.zeros((x.shape[0]))
                for PM in range(w.shape[0]):
                    y += x ** PM * w[PM]
                # plt.plot(x, y, label = "Đường dự đoán")
                # plt.title("Bậc: " + str(i - 1) + "\nTỉ lệ Train và Test: " + str(X.shape[0]) + " : " + str(X_data.shape[0] - X.shape[0]))
                # plt.xlabel("Tuổi")
                # plt.ylabel("Thể Lực")
                # plt.plot(X_data, Y_data, 'bo', label = "Dữ liệu Test")
                # plt.plot(X, Y, 'ro', label="Dữ liệu thật là Train")
                # plt.legend()
                # plt.show()
                X_test = np.copy(X_data)
                Y_test = np.zeros((X_test.shape[0], X_test.shape[1]))
                for PM in range(w.shape[0]):
                    Y_test += X_test ** PM * w[PM]
                Y_data_fake = Y_data.reshape(Y_data.shape[0])
                Y_test = Y_test.reshape(Y_test.shape[0])
                MSE = mean_absolute_error(Y_data_fake, Y_test)
                sw = ""
                if MSE < r[-1][-1]:
                    for m in range(w.shape[0]):
                        sw += str(w[m]) + " "
                    r.append((sw, MSE))
    return r


r = linear_regression(X, Y, n = 5)

if len(r) != 0:
    with open("C:/Users/DD/Desktop/Project/GitLab/machine-learning-basic-2/Selected_Models.txt", "a") as f:
            f.write(str(r[-1][0]))
            f.write("|")
            f.write(str(r[-1][1]) + "\n")

r_cols = ["w", "MSE"]
rfile = pd.read_csv("C:/Users/DD/Desktop/Project/GitLab/machine-learning-basic-2/Selected_Models.txt", sep = "|", names = r_cols)
rf = np.asarray(rfile)
MSE = 10 ** 9
pos = 0
for i in range(rf.shape[0]):
    if rf[i][1] < MSE:
        pos = i
        MSE = rf[i][1]
print("W tốt nhất: " + str(rf[pos][0]))
print("Sai số bình phương nhỏ nhất: " + str(MSE))
w = rf[pos][0].split(" ")
w = w[:-1]
for i in range(len(w)):
    w[i] = float(w[i][1:(len(w[i]) - 2)])
X_draw = np.linspace(50, 90, 10000)
Y_draw = np.zeros((X_draw.shape[0]))
for p in range(len(w)):
    Y_draw += X_draw ** p * w[p]
plt.plot(X_draw, Y_draw)
plt.title("Đường dự đoán tốt nhất")
plt.xlabel("Tuổi")
plt.ylabel("Điểm thể lực")
plt.plot(X, Y, "ro")
plt.show()


