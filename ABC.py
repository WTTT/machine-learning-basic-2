import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from emnist import extract_training_samples
from sklearn.cluster import KMeans
from sklearn import neighbors
from sklearn.preprocessing import normalize

# Train data từ MNIST
images, labels = extract_training_samples('letters')
# XỬ Lý Dữ Liệu
imgs = []
small_imgs = []
small_lbs = []

for i in range(int(images.shape[0] / 10)): # sửa số 10 thành 1 để lấy tất cả các training dữ liệu
    if labels[i] == 1 or labels[i] == 2 or labels[i] == 3:
        imgs.append(images[i].reshape(1, 28 * 28))
        small_imgs.append(images[i])
        small_lbs.append(labels[i])

ABC = np.zeros((len(imgs), 28 * 28))

for i in range(len(imgs)):
    ABC[i] = imgs[i].reshape(1, 28 * 28)

# Sử dụng KNN để training dữ liệu
knn = neighbors.KNeighborsClassifier(n_neighbors=10, p=12, weights='distance')
knn.fit(ABC, small_lbs)

# Test Data
from emnist import extract_test_samples
nimages, nlabels = extract_test_samples('letters')
# Xứ lý dữ liệu
nimgs = []
nsmall_imgs = []
nsmall_lbs = []
for i in range(int(nimages.shape[0] / 10)):
    nimgs.append(nimages[i].reshape(1, 28 * 28))
    nsmall_imgs.append(nimages[i])
    nsmall_lbs.append(nlabels[i])

nABC = np.zeros((len(nimgs), 28 * 28))

for i in range(len(nimgs)):
    nABC[i] = nimgs[i].reshape(1, 28 * 28)

pre_labels = knn.predict(nABC)



for i in range(len(pre_labels)):
    if pre_labels[i] == 3:
        plt.title("C")
        plt.imshow(nsmall_imgs[i], interpolation="nearest")
        plt.axis('off')
        plt.show()
    if pre_labels[i] == 1:
        plt.title("A")
        plt.imshow(nsmall_imgs[i], interpolation="nearest")
        plt.axis('off')
        plt.show()
    if pre_labels[i] == 2:
        plt.title("B")
        plt.imshow(nsmall_imgs[i], interpolation="nearest")
        plt.axis('off')
        plt.show()
())